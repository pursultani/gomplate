module gitlab.com/pursultani/gomplate

go 1.18

require (
	github.com/Masterminds/goutils v1.1.1
	github.com/Shopify/ejson v1.3.3
	github.com/apparentlymart/go-cidr v1.1.0
	github.com/google/uuid v1.3.0
	github.com/gosimple/slug v1.13.1
	github.com/hairyhenderson/go-fsimpl v0.0.0-20220910230923-9fc15c316e74
	github.com/hairyhenderson/toml v0.4.2-0.20210923231440-40456b8e66cf
	github.com/hairyhenderson/yaml v2.1.0+incompatible
	github.com/hashicorp/go-sockaddr v1.0.2
	github.com/joho/godotenv v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.28.0
	github.com/spf13/afero v1.9.2
	github.com/spf13/cobra v1.6.0
	github.com/stretchr/testify v1.8.0
	github.com/ugorji/go/codec v1.2.7
	github.com/zealic/xignore v0.3.3
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
	golang.org/x/sys v0.0.0-20220913120320-3275c407cedc
	golang.org/x/term v0.0.0-20220722155259-a9ba230a4035
	golang.org/x/text v0.3.8
	gopkg.in/yaml.v3 v3.0.1
	gotest.tools/v3 v3.4.0
	inet.af/netaddr v0.0.0-20220811202034-502d2d690317
	k8s.io/client-go v0.25.3
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dustin/gojson v0.0.0-20160307161227-2e71ec9dd5ad // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/gosimple/unidecode v1.0.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	go4.org/intern v0.0.0-20211027215823-ae77deb06f29 // indirect
	go4.org/unsafe/assume-no-moving-gc v0.0.0-20220617031537-928513b29760 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
